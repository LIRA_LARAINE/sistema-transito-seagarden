DROP TABLE BD_Error;
DROP TABLE transito;
DROP TABLE pieza;
DROP TABLE unidad;
ALTER TABLE calibre DROP COLUMN cali_min;
ALTER TABLE calibre DROP COLUMN cali_max;
DROP PROC SP_ListarTurnos;
DROP PROC SP_ListarLote;
DROP PROC SP_ListarCalibre;
DROP PROC SP_ListarEspecie;
DROP PROC SP_ListarCorte;
DROP PROC SP_ListarTerminacion;
DROP PROC SP_ListarCalidad;
DROP PROC SP_ListarConservacion;
DROP PROC SP_ListarPieza;
DROP PROC SP_ListarUnidad;
DROP PROC SP_ListarTransito;
DROP PROC SP_InsertarTransito;
DROP PROC SP_ValidaIP;
DROP SEQUENCE SQ_transito
DROP SEQUENCE SQ_errores
------------------------------------CREA TABLAS---------------------------------
---AGREGAR COLUMNAS A TABLA CALIBRE---
ALTER TABLE calibre
ADD cali_min FLOAT NULL, 
	cali_max FLOAT NULL;


---CREA SECUENCIA PARA TABLA TRANSITO---
CREATE SEQUENCE SQ_transito START WITH 1 INCREMENT BY 1;


---CREA SECUENCIA PARA TABLA BD_Error---
CREATE SEQUENCE SQ_errores START WITH 1 INCREMENT BY 1;


---CREAR TABLA ERRORES---
--SELECT * FROM master.dbo.sysmessages
CREATE TABLE BD_Error
(
	ID_Error			INT NOT NULL,
	Nom_Usuario			VARCHAR(100),
	Nro_Error			INT,
	Estado_Error		INT,
	Gravedad_Error		INT,
	Linea_Error			INT,
	Procedimiento_Error	VARCHAR(MAX),
	Mensaje_Error		VARCHAR(MAX),
	Fecha_Error			DATETIME
);


---CREAR TABLA PIEZA---
CREATE TABLE pieza
(
	cod_pieza	SMALLINT PRIMARY KEY not null,  
	codigo		INT null, 
	inactivo	NUMERIC(18,0)
);


---INSERTAR DATOS TABLA PIEZA---
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (1, 1, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (2, 2, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (3, 3, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (4, 4, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (5, 5, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (6, 6, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (7, 7, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (8, 8, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (9, 9, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (10, 10, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (11, 11, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (12, 12, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (13, 13, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (14, 14, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (15, 15, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (16, 16, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (17, 17, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (18, 18, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (19, 19, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (20, 20, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (21, 21, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (22, 22, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (23, 23, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (24, 24, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (25, 25, 0);


---CREA TABLA UNIDAD---
CREATE TABLE unidad
(
	cod_uni		SMALLINT PRIMARY KEY not null,  
	codigo		VARCHAR(5) null,
	nombre		VARCHAR(50) null,
	inactivo	NUMERIC(18,0)
);


---INSERTAR DATOS TABLA UNIDAD---
INSERT INTO unidad (cod_uni, codigo, nombre, inactivo) VALUES (1,'BAN','BANDEJA',0);
INSERT INTO unidad (cod_uni, codigo, nombre, inactivo) VALUES (2,'CAJ','CAJA',0);
INSERT INTO unidad (cod_uni, codigo, nombre, inactivo) VALUES (3,'BOL','BOLSA',0);


---CREATE TABLA TRANSITO---
CREATE TABLE transito
(
	cod_transito INT IDENTITY(1,1) NOT NULL,
	nom_equipo varchar(50),
	id_turno SMALLINT,
	id_lote BIGINT,
	id_calibre SMALLINT,
	id_especie SMALLINT,
	id_corte SMALLINT,
	id_terminacion SMALLINT,
	id_calidad SMALLINT,
	id_conservacion SMALLINT,
	id_pieza SMALLINT,
	id_unidad SMALLINT,
	kilos FLOAT
);

---RELACIONES TABLA TRANSITO---
ALTER TABLE transito
	ADD CONSTRAINT cod_transito_PK PRIMARY KEY (cod_transito);

ALTER TABLE transito
	ADD CONSTRAINT id_turno_FK FOREIGN KEY (id_turno) REFERENCES turno (CodTurno);

ALTER TABLE transito
	ADD CONSTRAINT id_lote_FK FOREIGN KEY (id_lote) REFERENCES lotes (cod_lote);

ALTER TABLE transito
	ADD CONSTRAINT id_calibre_FK FOREIGN KEY (id_calibre) REFERENCES calibre (cod_calib);

ALTER TABLE transito
	ADD CONSTRAINT id_especie_FK FOREIGN KEY (id_especie) REFERENCES especies (cod_especie);

ALTER TABLE transito
	ADD CONSTRAINT id_corte_FK FOREIGN KEY (id_corte) REFERENCES corte (cod_corte);

ALTER TABLE transito
	ADD CONSTRAINT id_terminacion_FK FOREIGN KEY (id_terminacion) REFERENCES terminacion (cod_term);

ALTER TABLE transito
	ADD CONSTRAINT id_calidad_FK FOREIGN KEY (id_calidad) REFERENCES calidad (cod_cald);
	
ALTER TABLE transito
	ADD CONSTRAINT id_conservacion_FK FOREIGN KEY (id_conservacion) REFERENCES conservacion (cod_cons);

ALTER TABLE transito
	ADD CONSTRAINT id_pieza_FK FOREIGN KEY (id_pieza) REFERENCES pieza (cod_pieza);

ALTER TABLE transito
	ADD CONSTRAINT id_unidad_FK FOREIGN KEY (id_unidad) REFERENCES unidad (cod_uni);
	select * from transito

-------------------------------------CREA PROCEDIMIENTOS------------------------------------
---CREA PROCEDIMIENTO PARA LISTAR ROTULADOS---
CREATE OR ALTER PROC SP_ListarTransito 
AS
BEGIN
SELECT TOP 20 
	cod_transito AS ID, 
	nom_equipo AS EQUIPO, 
	turno.NomTurno AS TURNO, 
	lotes.nombre AS LOTE, 
	calibre.nombre AS CALIBRE,
	especies.descripcion AS ESPECIE, 
	corte.nombre AS CORTE, 
	terminacion.nombre AS TERMINACION, 
	calidad.nombre AS CALIDAD, 
	conservacion.nombre AS CONSERVACION, 
	pieza.codigo AS PIEZAS, 
	unidad.nombre AS UNIDAD, 
	kilos AS PESO,
	format(convert(datetime, lotes.reg_cosecha),'dd/MM/yyyy') AS FCH_ELAB
FROM transito INNER JOIN turno ON transito.id_turno=turno.CodTurno
	INNER JOIN lotes ON transito.id_lote=lotes.cod_lote
	INNER JOIN calibre ON transito.id_calibre=calibre.cod_calib
	INNER JOIN especies ON transito.id_especie=especies.cod_especie
	INNER JOIN corte ON transito.id_corte=corte.cod_corte
	INNER JOIN terminacion ON transito.id_terminacion=terminacion.cod_term
	INNER JOIN calidad ON transito.id_calidad=calidad.cod_cald
	INNER JOIN conservacion ON transito.id_conservacion=conservacion.cod_cons
	INNER JOIN pieza ON transito.id_pieza=pieza.cod_pieza
	INNER JOIN unidad ON transito.id_unidad=unidad.cod_uni
	ORDER BY cod_transito DESC 
END
GO

exec SP_ListarTransito


---CREA PROCEDIMIENTO PARA LISTAR TURNOS---
CREATE OR ALTER PROC SP_ListarTurnos
AS 
SELECT CodTurno, NomTurno FROM turno WHERE inactivo=0 ORDER BY NomTurno ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR LOTE---
CREATE OR ALTER PROC SP_ListarLote
AS 
SELECT cod_lote, nombre FROM lotes WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR CALIBRE--
CREATE OR ALTER PROC SP_ListarCalibre
AS 
SELECT cod_calib, nombre FROM calibre WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR ESPECIE---
CREATE OR ALTER PROC SP_ListarEspecie
AS 
SELECT cod_especie, descripcion FROM especies WHERE inactivo=0 ORDER BY descripcion ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR CORTE---
CREATE OR ALTER PROC SP_ListarCorte
AS 
SELECT cod_corte, nombre FROM corte WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR TERMINACIÓN---
CREATE OR ALTER PROC SP_ListarTerminacion
AS 
SELECT cod_term, nombre FROM terminacion WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR CALIDAD---
CREATE OR ALTER PROC SP_ListarCalidad
AS 
SELECT cod_cald, nombre FROM calidad WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR CONSERVACIÓN---
CREATE OR ALTER PROC SP_ListarConservacion
AS 
SELECT cod_cons, nombre FROM conservacion WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR PIEZA---
CREATE OR ALTER PROC SP_ListarPieza
AS 
SELECT cod_pieza, codigo FROM pieza WHERE inactivo=0 ORDER BY codigo ASC
GO


---CREA PROCEDIMIENTO PARA LISTAR UNIDAD---
CREATE OR ALTER PROC SP_ListarUnidad
AS 
SELECT cod_uni, nombre FROM unidad WHERE inactivo=0 ORDER BY nombre ASC
GO


---CREA PROCEDIMIENTO PARA INSERTAR ROTULADOS---
CREATE OR ALTER PROC SP_InsertarTransito
@equipo varchar (50),
@turno INT,
@lote INT,
@calibre INT,
@especie INT,
@corte INT,
@terminacion INT,
@calidad INT,
@conservacion INT,
@pieza INT,
@unidad INT,
@kilo FLOAT
AS
DECLARE @MInsert VARCHAR(250)
BEGIN
	BEGIN TRY
		BEGIN TRANSACTION
			INSERT INTO transito 
			VALUES (@equipo,@turno,@lote,@calibre,@especie,@corte,@terminacion,@calidad,@conservacion,@pieza,@unidad,@kilo)
			set @MInsert='Registro grabado'
		COMMIT TRANSACTION
	END TRY
	BEGIN CATCH
		set @MInsert = ERROR_MESSAGE()
	END CATCH
END
GO


--- CREA PROCEDIMIENTO PARA OBTENER IP LOCAL DE EQUIPO Y COMPARAR CON TABLA EQUIPO---
CREATE OR ALTER PROC SP_ValidaIP
AS
BEGIN
	SELECT nombre
	FROM sys.dm_exec_connections JOIN equipo on parametro=
		(SELECT client_net_address FROM sys.dm_exec_connections WHERE session_id = @@SPID)
	GROUP BY nombre
END
GO


---CREA FUNCIÓN PARA CALCULAR KILOS---
CREATE OR ALTER FUNCTION FN_CalculoKilos (@calibre SMALLINT, @piezas SMALLINT)
RETURNS FLOAT
AS
BEGIN
	DECLARE @kilos AS FLOAT, @cali_min AS FLOAT, @cali_max AS FLOAT
	IF @calibre = 0 OR @piezas= 0
	BEGIN
	SET @kilos=0
	RETURN @kilos
	END
	SET @cali_min = (SELECT cali_min FROM calibre WHERE cod_calib=@calibre)
	SET @cali_max = (SELECT cali_max FROM calibre WHERE cod_calib=@calibre)
	SET @kilos = ((@cali_min + @cali_max)/2)*@piezas 
	SET @kilos = ISNULL (@kilos,0)
	RETURN @kilos 
END
GO