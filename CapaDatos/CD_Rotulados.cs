using System;

using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Runtime.ExceptionServices;
using System.ComponentModel;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Net.NetworkInformation;
using System.Globalization;
using System.Security.Policy;

namespace CapaDatos
{
    ///Clase de acceso a la base de datos - Capa de DATOS
    public class CD_Rotulados
    {
        ///Instancias de comandos
        private CD_Conexion conexion = new CD_Conexion();
        SqlDataReader leer;
        DataTable tabla = new DataTable();
        SqlCommand comando = new SqlCommand();

        ///M�todo para llamar a SP de Listar tabla Tr�nsito para DataGridView
        public DataTable MostrarTransito()
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarTransito";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar Nombre de equipo para TextBox
        public DataTable NombreEquipo()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ValidaIP";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Turno para ComboBox
        public DataTable ListarTurno()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarTurnos";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Lote para ComboBox
        public DataTable ListarLote()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarLote";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Corte para ComboBox
        public DataTable ListarCorte()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarCorte";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Calidad para ComboBox
        public DataTable ListarCalidad()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarCalidad";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Conservaci�n para ComboBox
        public DataTable ListarConservacion()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarConservacion";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Calibre para ComboBox
        public DataTable ListarCalibre()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarCalibre";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Pieza para ComboBox
        public DataTable ListarPieza()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarPieza";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Unidad para ComboBox
        public DataTable ListarUnidad()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarUnidad";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla LoEspecie para ComboBox
        public DataTable ListarEspecie()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarEspecie";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Terminaci�n para ComboBox
        public DataTable ListarTerminacion()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarTerminacion";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para llamar a SP de Listar tabla Etiqueta para ComboBox
        public DataTable ListarEtiqueta()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_ListarEtiqueta";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///M�todo para Insertar parametros obtenidos en capa de negocio mediante SP a la tabla Tr�nsito
        public string InsertarTransito(string equipo, int turno, int lote, int calibre, int especie, int corte, int terminacion, int calidad, int conservacion, int pieza, int unidad, int etiqueta, double kilos)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_InsertarTransito";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@equipo", equipo);
            comando.Parameters.AddWithValue("@turno", turno);
            comando.Parameters.AddWithValue("@lote", lote);
            comando.Parameters.AddWithValue("@calibre", calibre);
            comando.Parameters.AddWithValue("@especie", especie);
            comando.Parameters.AddWithValue("@corte", corte);
            comando.Parameters.AddWithValue("@terminacion", terminacion);
            comando.Parameters.AddWithValue("@calidad", calidad);
            comando.Parameters.AddWithValue("@conservacion", conservacion);
            comando.Parameters.AddWithValue("@pieza", pieza);
            comando.Parameters.AddWithValue("@unidad", unidad);
            comando.Parameters.AddWithValue("@etiqueta", etiqueta);
            comando.Parameters.AddWithValue("@kilo", kilos);
            comando.Parameters.Add("@MsjInsertar", SqlDbType.Int).Direction = ParameterDirection.ReturnValue;
            comando.ExecuteNonQuery();
            string mensaje = comando.Parameters["@MsjInsertar"].Value.ToString();
            conexion.CerrarConexion();
            return mensaje;
        }

        ///M�todo para llamar a FN de C�lculo de Kilos con retorno para TextBox
        public double ActivaFuncionCalculaKilos(int calibre, int pieza)
        {
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "dbo.FN_CalculoKilos";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.Clear();
            comando.Parameters.AddWithValue("@calibre", calibre);
            comando.Parameters.AddWithValue("@piezas", pieza);
            comando.Parameters.Add("@kilos", SqlDbType.Float).Direction = ParameterDirection.ReturnValue;
            comando.ExecuteNonQuery();
            double kilos = double.Parse(comando.Parameters["@kilos"].Value.ToString());
            conexion.CerrarConexion();
            return kilos;
        }

        ///M�todo para llamar a SP con el c�digo y etiqueta de �ltimo registro de tabla tr�nsito
        public DataTable TraeUltimoRegTransito()
        {
            DataTable tabla = new DataTable();
            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_UltimoRegTransito";
            comando.CommandType = CommandType.StoredProcedure;
            leer = comando.ExecuteReader();
            tabla.Load(leer);
            conexion.CerrarConexion();
            return tabla;
        }

        ///Met�do para llamar a etiqueta de tr�nsito
        public String getEtiquetaTransito(int cod_transito, int cod_etiqueta)
        {
            String resultado = "";

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = "SP_CrearEtiquetaTRN";
            comando.CommandType = CommandType.StoredProcedure;
            comando.Parameters.AddWithValue("@IdCaja", cod_transito);
            comando.Parameters.AddWithValue("@IdEtiqueta", cod_etiqueta);
            SqlDataReader reader = comando.ExecuteReader();
            while (reader.Read())
            {
                resultado = resultado + reader.GetString(0) + Environment.NewLine;
            }
            reader.Close();
            return resultado;
        }
    }
}
