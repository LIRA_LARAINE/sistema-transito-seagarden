﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace CapaDatos
{
    ///Clase única de conexión a la base de datos
    public class CD_Conexion
    {
        ///Cadena de conexión
        private SqlConnection Conexion = new SqlConnection("Server=DESKTOP-8OJFST1;DataBase=bdsystem;Integrated Security=true");

        ///Apertura de conexión
        public SqlConnection AbrirConexion()
        {
            if (Conexion.State == ConnectionState.Closed)
                Conexion.Open();
            return Conexion;
        }

        ///Cierre de conexión
        public SqlConnection CerrarConexion()
        {
            if (Conexion.State == ConnectionState.Open)
                Conexion.Close();
            return Conexion;
        }
    }
}
