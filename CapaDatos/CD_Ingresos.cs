﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class CD_Ingresos
    {

        private static CD_Conexion conexion = new CD_Conexion();
        private static SqlCommand comando = new SqlCommand();
        private static SqlDataReader leer;


        public int id_transito { get; set; }

        public int id_sala { get; set; }
        public string nombre_sala { get; set; }

        public int id_camara { get; set; }
        public string nombre_camara { get; set; }

        public int id_etiqueta { get; set; }




        public CD_Ingresos() { }

        
      

        public static List<CD_Ingresos> GetCamara()
        {





            List<CD_Ingresos> lista = new List<CD_Ingresos>();

            comando.Connection = conexion.AbrirConexion();


            string sql = "select id_camara , rtrim(descripcion) from camaras where (tipo = 1) order by id_camara desc";

            comando.CommandText = sql;
            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();

            while (leer.Read())
            {

                CD_Ingresos cam = new CD_Ingresos();

                cam.id_camara = leer.GetInt32(0);
                cam.nombre_camara = leer.GetString(1);


                lista.Add(cam);

            }

            leer.Close();

            conexion.CerrarConexion();

            return lista;

        }







        public static List<CD_Ingresos> GetOrigen()
        {



            List<CD_Ingresos> lista = new List <CD_Ingresos>();

            comando.Connection = conexion.AbrirConexion();


            string sql = "select id_sala , nombre_sala from sala_origen order by id_sala desc";

            comando.CommandText = sql;
            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();

            while (leer.Read())
            {

                CD_Ingresos so = new CD_Ingresos();

                so.id_sala = leer.GetInt32(0);
                so.nombre_sala= leer.GetString(1);


                lista.Add(so);

            }

            leer.Close();

            conexion.CerrarConexion();


            return lista;

        }

         

        public int GetIdEtiqueta (CD_Ingresos ci)
        {

            int resultado = 0;

            string sql = "Select id_etiqueta from transito where cod_transito ="+ ci.id_transito ;

            comando.Connection = conexion.AbrirConexion();
            comando.CommandText = sql;
            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();

            leer.Read();
            resultado = leer.GetInt16(0);
            leer.Close();

            conexion.CerrarConexion();

            return resultado;

        }





        public  string  GuardarIngreso(CD_Ingresos ci)
        {

            string resultado = "";

            string sql = "SP_IngresaTRN " + ci.id_transito + "," + ci.id_sala + "," + ci.id_camara;


            comando.Connection = conexion.AbrirConexion();

            comando.CommandText = sql;

            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();

            leer.Read();
            resultado = leer.GetString(0);
            leer.Close();

            conexion.CerrarConexion();

            return resultado;


        }



        public String GetEtiquetaZPL(CD_Ingresos ci)
        {

            String resultado = "";

            comando.Connection = conexion.AbrirConexion();




            string sql = "SP_CrearEtiquetaTRN " + ci.id_transito + "," + ci.id_etiqueta;


            comando.Connection = conexion.AbrirConexion();

            comando.CommandText = sql;

            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();



            while (leer.Read())
            {

                resultado = resultado + leer.GetString(0) + Environment.NewLine;

            }

            leer.Close();

            conexion.CerrarConexion();

            return resultado;

        }



    }
}
