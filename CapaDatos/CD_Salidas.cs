﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CapaDatos
{
    public class CD_Salidas
    {
        private static CD_Conexion conexion = new CD_Conexion();
        private static SqlCommand comando = new SqlCommand();
        private static SqlDataReader leer;


        public int id_transito { get; set; }
        public int id_motivo { get; set; }
        public string nombre_motivo { get; set; }


        public static List<CD_Salidas> GetMotivo()
        {


            List<CD_Salidas> lista = new List<CD_Salidas>();

            comando.Connection = conexion.AbrirConexion();


            string sql = "select id_motivo , rtrim(nombre_motivo) from motivo_desp where (inactivo = 0) order by id_motivo desc";

            comando.CommandText = sql;
            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();

            while (leer.Read())
            {

                CD_Salidas mt = new CD_Salidas();

                mt.id_motivo = leer.GetInt32(0);
                mt.nombre_motivo = leer.GetString(1);


                lista.Add(mt);

            }

            leer.Close();

            conexion.CerrarConexion();

            return lista;

        }



        public string GuardarSalida(CD_Salidas cs)
        {

            string resultado = "";

            string sql = "SP_SalidaTRN " + cs.id_transito + "," + cs.id_motivo ;


            comando.Connection = conexion.AbrirConexion();

            comando.CommandText = sql;

            comando.CommandType = CommandType.Text;

            leer = comando.ExecuteReader();

            leer.Read();
            resultado = leer.GetString(0);
            leer.Close();

            conexion.CerrarConexion();

            return resultado;


        }


    }
}
