USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarCorte]    Script Date: 08-08-2020 12:19:33 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Corte
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarCorte]
AS 
SELECT cod_corte, nombre 
FROM corte WHERE 
inactivo=0 
ORDER BY nombre ASC
