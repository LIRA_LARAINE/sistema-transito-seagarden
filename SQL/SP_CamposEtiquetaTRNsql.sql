USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_CamposEtiquetaTRN]    Script Date: 08-08-2020 13:11:57 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Marcos Campos 
-- Create date: 04/08/2020
-- Description:	Campos Impresion Etiqueta Transito
-- =============================================
ALTER PROCEDURE [dbo].[SP_CamposEtiquetaTRN]
(
@IdCaja int
)
AS
BEGIN	
SELECT	dbo.transito.cod_transito As IdCaja, 
		dbo.especies.descripcion AS N_Especie, 
		dbo.empresas.descripcion AS N_Empresa, 
		dbo.turno.NomTurno AS N_Turno, 
		dbo.lotes.nombre AS N_Lote, 
		Format(CONVERT(datetime, dbo.lotes.reg_cosecha), 'dd-MM-yyyy') AS N_FechaProd, 
		dbo.calibre.nombre AS N_Calibre, 
		dbo.corte.nombre AS N_Corte, 
		dbo.terminacion.nombre AS N_Terminacion, 
		dbo.calidad.nombre AS N_Calidad, 
		dbo.conservacion.nombre AS N_Conservacion, 
        dbo.unidad.nombre AS N_Unidad, 
		dbo.pieza.codigo AS N_Piezas, 
		dbo.transito.kilos AS N_Kilos, 
		0 As N_Usuario

FROM	dbo.transito INNER JOIN
        dbo.turno ON dbo.transito.id_turno = dbo.turno.CodTurno INNER JOIN
        dbo.corte ON dbo.transito.id_corte = dbo.corte.cod_corte INNER JOIN
        dbo.calidad ON dbo.transito.id_calidad = dbo.calidad.cod_cald LEFT OUTER JOIN
        dbo.pieza ON dbo.transito.id_pieza = dbo.pieza.cod_pieza LEFT OUTER JOIN
        dbo.unidad ON dbo.transito.id_unidad = dbo.unidad.cod_uni LEFT OUTER JOIN
        dbo.conservacion ON dbo.transito.id_conservacion = dbo.conservacion.cod_cons LEFT OUTER JOIN
        dbo.terminacion ON dbo.transito.id_terminacion = dbo.terminacion.cod_term LEFT OUTER JOIN
        dbo.lotes ON dbo.transito.id_lote = dbo.lotes.cod_lote LEFT OUTER JOIN
        dbo.calibre ON dbo.transito.id_calibre = dbo.calibre.cod_calib LEFT OUTER JOIN
        dbo.empresas ON dbo.lotes.cod_empresa = dbo.empresas.cod_empresa LEFT OUTER JOIN
        dbo.especies ON dbo.transito.id_especie = dbo.especies.cod_especie
WHERE	(dbo.transito.cod_transito = 34)
END
