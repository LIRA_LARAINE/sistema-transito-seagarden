USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarPieza]    Script Date: 08-08-2020 12:23:11 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Pieza
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarPieza]
AS 
SELECT cod_pieza, codigo 
FROM pieza 
WHERE inactivo=0 
ORDER BY codigo ASC
