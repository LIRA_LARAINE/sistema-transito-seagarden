USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarCalidad]    Script Date: 08-08-2020 12:20:53 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Calidad
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarCalidad]
AS 
SELECT cod_cald, nombre 
FROM calidad 
WHERE inactivo=0 
ORDER BY nombre ASC