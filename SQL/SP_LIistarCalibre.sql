USE [bdsystem]
GO
/****** Object:  StoredProcedure [dbo].[SP_ListarCalibre]    Script Date: 14-08-2020 13:16:35 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ARITHABORT ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Calibre
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarCalibre]
AS 
SELECT cod_calib, nombre 
FROM calibre
WHERE inactivo=0 
ORDER BY nombre ASC
