USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarConservacion]    Script Date: 08-08-2020 12:22:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Conservación
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarConservacion]
AS 
SELECT cod_cons, nombre 
FROM conservacion 
WHERE inactivo=0 
ORDER BY nombre ASC
