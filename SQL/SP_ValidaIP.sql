USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ValidaIP]    Script Date: 08-08-2020 12:28:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Obtiene y compara IP de equipo
-- =============================================
ALTER   PROC [dbo].[SP_ValidaIP]
AS
BEGIN
	SELECT nombre
	FROM sys.dm_exec_connections JOIN equipo on parametro=
		(SELECT client_net_address FROM sys.dm_exec_connections WHERE session_id = @@SPID)
	GROUP BY nombre
END
