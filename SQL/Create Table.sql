DROP TABLE transito;
DROP TABLE pieza;
DROP TABLE unidad;
DROP TABLE BD_Error;
ALTER TABLE calibre DROP COLUMN cali_min;
ALTER TABLE calibre DROP COLUMN cali_max;
------------------------------------CREA TABLAS---------------------------------
---AGREGAR COLUMNAS A TABLA CALIBRE---
ALTER TABLE calibre
ADD cali_min FLOAT NULL, 
	cali_max FLOAT NULL;


---CREAR TABLA PIEZA---
CREATE TABLE pieza
(
	cod_pieza	SMALLINT PRIMARY KEY NOT NULL,  
	codigo		INT NOT NULL, 
	inactivo	NUMERIC(18,0) NOT NULL
);


---INSERTAR DATOS TABLA PIEZA---
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (1, 1, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (2, 2, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (3, 3, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (4, 4, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (5, 5, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (6, 6, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (7, 7, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (8, 8, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (9, 9, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (10, 10, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (11, 11, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (12, 12, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (13, 13, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (14, 14, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (15, 15, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (16, 16, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (17, 17, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (18, 18, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (19, 19, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (20, 20, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (21, 21, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (22, 22, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (23, 23, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (24, 24, 0);
INSERT INTO pieza (cod_pieza, codigo, inactivo) VALUES (25, 25, 0);


---CREA TABLA UNIDAD---
CREATE TABLE unidad
(
	cod_uni		SMALLINT PRIMARY KEY NOT NULL,  
	codigo		VARCHAR(5) NOT NULL,
	nombre		VARCHAR(50) NOT NULL,
	inactivo	NUMERIC(18,0) NOT NULL
);


---INSERTAR DATOS TABLA UNIDAD---
INSERT INTO unidad (cod_uni, codigo, nombre, inactivo) VALUES (1,'BAN','BANDEJA',0);
INSERT INTO unidad (cod_uni, codigo, nombre, inactivo) VALUES (2,'CAJ','CAJA',0);
INSERT INTO unidad (cod_uni, codigo, nombre, inactivo) VALUES (3,'BOL','BOLSA',0);


---CREATE TABLA TRANSITO---
CREATE TABLE transito
(
	cod_transito INT IDENTITY(1,1) NOT NULL,
	nom_equipo VARCHAR(50) NOT NULL,
	id_turno SMALLINT NOT NULL,
	id_lote BIGINT NOT NULL,
	id_calibre SMALLINT NOT NULL,
	id_especie SMALLINT NOT NULL,
	id_corte SMALLINT NOT NULL,
	id_terminacion SMALLINT NOT NULL,
	id_calidad SMALLINT NOT NULL,
	id_conservacion SMALLINT NOT NULL,
	id_pieza SMALLINT NOT NULL,
	id_unidad SMALLINT NOT NULL,
	id_etiqueta SMALLINT NOT NULL,
	kilos FLOAT NOT NULL
);

---RELACIONES TABLA TRANSITO---
ALTER TABLE transito
	ADD CONSTRAINT cod_transito_PK PRIMARY KEY (cod_transito);

ALTER TABLE transito
	ADD CONSTRAINT id_turno_FK FOREIGN KEY (id_turno) REFERENCES turno (CodTurno);

ALTER TABLE transito
	ADD CONSTRAINT id_lote_FK FOREIGN KEY (id_lote) REFERENCES lotes (cod_lote);

ALTER TABLE transito
	ADD CONSTRAINT id_calibre_FK FOREIGN KEY (id_calibre) REFERENCES calibre (cod_calib);

ALTER TABLE transito
	ADD CONSTRAINT id_especie_FK FOREIGN KEY (id_especie) REFERENCES especies (cod_especie);

ALTER TABLE transito
	ADD CONSTRAINT id_corte_FK FOREIGN KEY (id_corte) REFERENCES corte (cod_corte);

ALTER TABLE transito
	ADD CONSTRAINT id_terminacion_FK FOREIGN KEY (id_terminacion) REFERENCES terminacion (cod_term);

ALTER TABLE transito
	ADD CONSTRAINT id_calidad_FK FOREIGN KEY (id_calidad) REFERENCES calidad (cod_cald);
	
ALTER TABLE transito
	ADD CONSTRAINT id_conservacion_FK FOREIGN KEY (id_conservacion) REFERENCES conservacion (cod_cons);

ALTER TABLE transito
	ADD CONSTRAINT id_pieza_FK FOREIGN KEY (id_pieza) REFERENCES pieza (cod_pieza);

ALTER TABLE transito
	ADD CONSTRAINT id_unidad_FK FOREIGN KEY (id_unidad) REFERENCES unidad (cod_uni);

ALTER TABLE transito
	ADD CONSTRAINT id_etiqueta_FK FOREIGN KEY (id_etiqueta) REFERENCES mae_etiqueta (cod_meti);