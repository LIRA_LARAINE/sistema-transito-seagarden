USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarTerminacion]    Script Date: 08-08-2020 12:20:10 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Terminación
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarTerminacion]
AS 
SELECT cod_term, nombre 
FROM terminacion 
WHERE inactivo=0 
ORDER BY nombre ASC
