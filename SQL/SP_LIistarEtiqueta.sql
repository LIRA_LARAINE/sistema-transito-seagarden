USE [bdsystem]
GO

SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 10/08/2020
-- Description:	Listar registros de tabla Etiqueta
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarEtiqueta]
AS 
SELECT cod_meti, nombre
FROM mae_etiqueta 
WHERE inactivo=0 AND individual=2
ORDER BY nombre ASC
