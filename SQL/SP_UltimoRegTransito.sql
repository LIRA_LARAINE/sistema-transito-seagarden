USE [bdsystem]
GO
/****** Object:  StoredProcedure [dbo].[SP_UltimoRegTransito]    Script Date: 12-08-2020 18:11:41 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 12/08/2020
-- Description:	Recupera código y etiqueta de último registro tránsito
-- =============================================
ALTER PROCEDURE [dbo].[SP_UltimoRegTransito]
AS
BEGIN
	SELECT cod_transito, id_etiqueta FROM transito WHERE cod_transito = (SELECT MAX(cod_transito) FROM transito);
END
GO
