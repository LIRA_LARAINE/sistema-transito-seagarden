USE [bdsystem]
GO

/****** Object:  UserDefinedFunction [dbo].[FN_CalculoKilos]    Script Date: 08-08-2020 12:30:49 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Recibe calibre y piezas para retornar cantidad de kilos
-- =============================================
ALTER FUNCTION [dbo].[FN_CalculoKilos] 
(
@calibre SMALLINT, 
@piezas SMALLINT
)
RETURNS FLOAT
AS
BEGIN
	DECLARE @kilos AS FLOAT, @cali_min AS FLOAT, @cali_max AS FLOAT
	IF @calibre = 0 OR @piezas= 0
	BEGIN
		SET @kilos=0
	RETURN @kilos
	END
	
	SET @cali_min = (SELECT cali_min FROM calibre WHERE cod_calib=@calibre)
	SET @cali_max = (SELECT cali_max FROM calibre WHERE cod_calib=@calibre)
	SET @kilos = ((@cali_min + @cali_max)/2)*@piezas 
	SET @kilos = ISNULL (@kilos,0)
	RETURN @kilos;
END