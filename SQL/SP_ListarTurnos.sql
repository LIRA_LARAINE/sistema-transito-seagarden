USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarTurnos]    Script Date: 08-08-2020 12:12:13 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira 
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Turnos
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarTurnos]
AS 
SELECT CodTurno, NomTurno 
FROM turno 
WHERE inactivo=0 
ORDER BY NomTurno ASC
