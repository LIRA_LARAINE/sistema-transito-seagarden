USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_CrearEtiquetaTRN]    Script Date: 08-08-2020 13:14:27 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Marcos Campos 
-- Create date: 04/08/2020
-- Description:	Campos Impresion Etiqueta Transito
-- =============================================
ALTER PROCEDURE [dbo].[SP_CrearEtiquetaTRN]
(
@IdCaja INT,
@IdEtiqueta AS INT
)
AS
BEGIN
DECLARE @str_text AS VARCHAR(1000),
		@empresa AS VARCHAR(100),
		@especie AS VARCHAR(50),
		@corte AS VARCHAR(50),
		@conservacion AS VARCHAR(50),
		@terminacion AS VARCHAR(50),
		@calidad AS VARCHAR(50),
		@calibre AS VARCHAR(50),
		@Unidad AS VARCHAR(50),
		@lote AS VARCHAR(50),
		@fechaprod AS VARCHAR(10),
		@kilos AS VARCHAR(6),
		@piezas AS VARCHAR(3),
		@turno AS VARCHAR(50),
		@usuario AS VARCHAR(50),
		@fechaimpresion AS DATETIME,
		@str_linea AS VARCHAR(MAX)

DECLARE @TablaTemporal TABLE (idlinea INT, texto VARCHAR(1000))

DECLARE @TablaEtiqueta TABLE(IdCaja INT PRIMARY KEY not null, 
							c_especie VARCHAR(50), 
							c_empresa VARCHAR(100),
							c_turno VARCHAR(50), 
							c_lote VARCHAR(50), 
							c_fechaprod VARCHAR(10), 
							c_calibre VARCHAR(50),
							c_corte VARCHAR(50), 
							c_terminacion VARCHAR(50), 
							c_calidad VARCHAR(50), 
							c_convervacion VARCHAR(50),
							c_unidad VARCHAR(50), 
							c_piezas VARCHAR(3), 
							c_kilos VARCHAR(5), 
							c_usuario VARCHAR(50))

INSERT INTO @TablaEtiqueta(IdCaja, 
			c_especie, 
			c_empresa,
			c_turno,
			c_lote,
			c_fechaprod,
			c_calibre,
			c_corte, 
			c_terminacion, 
			c_calidad, 
			c_convervacion, 
			c_unidad, 
			c_piezas, 
			c_kilos, 
			c_usuario)

EXEC SP_CamposEtiquetaTRN 0

SET @especie = (select c_especie from @TablaEtiqueta where IdCaja = @IdCaja)
SET @especie = ISNULL(@especie,'')

SET @empresa = (Select c_empresa from @TablaEtiqueta where IdCaja = @IdCaja)
SET @empresa = ISNULL(@empresa,'')

SET @turno = (Select c_turno from @TablaEtiqueta where IdCaja = @IdCaja)
SET @turno = ISNULL(@turno,'')

SET @lote = (Select c_lote from @TablaEtiqueta where IdCaja = @IdCaja)
SET @lote = ISNULL(@lote,'')

SET @fechaprod = (Select c_fechaprod from @TablaEtiqueta where IdCaja = @IdCaja)
SET @fechaprod = ISNULL(@fechaprod,'')

SET @calibre = (Select c_calibre from @TablaEtiqueta where IdCaja = @IdCaja)
SET @calibre = ISNULL(@calibre,'')

SET @corte = (Select c_corte from @TablaEtiqueta where IdCaja = @IdCaja)
SET @corte = ISNULL(@corte,'')

SET @terminacion = (Select c_terminacion from @TablaEtiqueta where IdCaja = @IdCaja)
SET @terminacion = ISNULL(@terminacion,'')

SET @calidad = (Select c_calidad from @TablaEtiqueta where IdCaja = @IdCaja)
SET @calidad = ISNULL(@calidad,'')

SET @conservacion = (Select c_convervacion from @TablaEtiqueta where IdCaja = @IdCaja)
SET @conservacion = ISNULL(@conservacion,'')

SET @Unidad = (Select c_unidad from @TablaEtiqueta where IdCaja = @IdCaja)
SET @Unidad = ISNULL(@Unidad,'')

SET @piezas = (Select c_piezas from @TablaEtiqueta where IdCaja = @IdCaja)
SET @piezas = ISNULL(@piezas,'')

SET @kilos = (Select c_kilos from @TablaEtiqueta where IdCaja = @IdCaja)
SET @kilos = ISNULL(@kilos,'')

SET @usuario = (Select c_usuario from @TablaEtiqueta where IdCaja = @IdCaja)
SET @usuario = ISNULL(@usuario,'')

SET @fechaimpresion = (select Format(CURRENT_TIMESTAMP,'dd-MM-yyyy HH:mm:ss'))

declare Etiqueta cursor for
	
Select str_text From det_etiqueta where (cod_meti IN(@IdEtiqueta)) 
ORDER BY cod_meti,n_linea

open Etiqueta

Fetch next from Etiqueta
into @str_text 
 
while @@fetch_status = 0
begin
	select @str_linea = replace(@str_text,'#CBARRA', @IdCaja)
	select @str_linea = replace(@str_linea,'#ESPECIE', @especie)
	select @str_linea = replace(@str_linea,'#TURNO', @turno)
	select @str_linea = replace(@str_linea,'#LOTE', @lote)
	select @str_linea = replace(@str_linea,'#FCH_PROD', @fechaprod)
	select @str_linea = replace(@str_linea,'#CALIBRE', @calibre)
	select @str_linea = replace(@str_linea,'#CORTE', @corte)
	select @str_linea = replace(@str_linea,'#TERMINACION', @terminacion)
	select @str_linea = replace(@str_linea,'#CALIDAD', @calidad)
	select @str_linea = replace(@str_linea,'#CONSERVACION', @conservacion)
	select @str_linea = replace(@str_linea,'#UNIDAD', @unidad)
	select @str_linea = replace(@str_linea,'#PIEZAS', @piezas)
	select @str_linea = replace(@str_linea,'#KILOS', @kilos)
	select @str_linea = replace(@str_linea,'#USUARIO', @usuario)
	select @str_linea = replace(@str_linea,'#FCH_IMPRESION', @fechaimpresion)

	Insert into @TablaTemporal(idlinea,texto)
	Values (0, @str_linea)

	fetch next from Etiqueta into @str_text

end
	
close Etiqueta
	
deallocate Etiqueta

Select texto From @TablaTemporal

END
