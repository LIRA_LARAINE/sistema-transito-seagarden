USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarEspecie]    Script Date: 08-08-2020 12:18:31 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Especie
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarEspecie]
AS 
SELECT cod_especie, descripcion
FROM especies 
WHERE inactivo=0 
ORDER BY descripcion ASC
