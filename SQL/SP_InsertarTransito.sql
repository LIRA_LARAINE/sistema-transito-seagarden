USE [bdsystem]
GO
/****** Object:  StoredProcedure [dbo].[SP_InsertarTransito]    Script Date: 08-08-2020 13:20:38 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Insertar registros a tabla Tránsito
-- =============================================
ALTER PROCEDURE [dbo].[SP_InsertarTransito]
(
@equipo varchar (50),
@turno INT,
@lote INT,
@calibre INT,
@especie INT,
@corte INT,
@terminacion INT,
@calidad INT,
@conservacion INT,
@pieza INT,
@unidad INT,
@etiqueta INT,
@kilo FLOAT
)
AS
DECLARE @MsjInsertar int
BEGIN
	BEGIN TRANSACTION
		INSERT INTO transito 
		VALUES (@equipo,@turno,@lote,@calibre,@especie,@corte,@terminacion,@calidad,@conservacion,@pieza,@unidad,@etiqueta,@kilo)
	COMMIT TRANSACTION
	SET @MsjInsertar = 1
RETURN @MsjInsertar
END
