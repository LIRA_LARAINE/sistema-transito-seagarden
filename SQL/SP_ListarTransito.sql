USE [bdsystem]
GO
/****** Object:  StoredProcedure [dbo].[SP_ListarTransito]    Script Date: 10-08-2020 23:53:57 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER  PROCEDURE [dbo].[SP_ListarTransito] 
AS
BEGIN
SELECT TOP 20	cod_transito AS ID, 
				nom_equipo AS EQUIPO, 
				turno.NomTurno AS TURNO, 
				lotes.nombre AS LOTE, 
				especies.descripcion AS ESPECIE,
				terminacion.nombre AS TERMINACION,
				conservacion.nombre AS CONSERVACION, 
				corte.nombre AS CORTE,
				calidad.nombre AS CALIDAD, 
				unidad.nombre AS UNIDAD,
				mae_etiqueta.nombre AS ETIQUETA,
				calibre.nombre AS CALIBRE,
				pieza.codigo AS PIEZAS, 
				kilos AS PESO,
				format(convert(datetime, lotes.reg_cosecha),'dd/MM/yyyy') AS FCH_ELAB

FROM	transito INNER JOIN turno ON transito.id_turno = turno.CodTurno
		INNER JOIN lotes ON transito.id_lote = lotes.cod_lote
		INNER JOIN especies ON transito.id_especie = especies.cod_especie
		INNER JOIN terminacion ON transito.id_terminacion = terminacion.cod_term
		INNER JOIN conservacion ON transito.id_conservacion = conservacion.cod_cons
		INNER JOIN corte ON transito.id_corte = corte.cod_corte
		INNER JOIN calidad ON transito.id_calidad = calidad.cod_cald
		INNER JOIN unidad ON transito.id_unidad = unidad.cod_uni
		INNER JOIN mae_etiqueta ON transito.id_etiqueta = mae_etiqueta.cod_meti
		INNER JOIN calibre ON transito.id_calibre = calibre.cod_calib
		INNER JOIN pieza ON transito.id_pieza = pieza.cod_pieza
		ORDER BY cod_transito DESC 
END