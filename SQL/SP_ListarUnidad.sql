USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_ListarUnidad]    Script Date: 08-08-2020 12:23:55 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Laraine Lira
-- Create date: 14/07/2020
-- Description:	Listar registros de tabla Unidad
-- =============================================
ALTER PROCEDURE [dbo].[SP_ListarUnidad]
AS 
SELECT cod_uni, nombre 
FROM unidad 
WHERE inactivo=0 
ORDER BY nombre ASC
