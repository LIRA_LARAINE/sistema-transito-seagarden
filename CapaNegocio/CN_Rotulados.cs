﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CapaDatos;
using System.ComponentModel;
using System.Runtime.InteropServices.WindowsRuntime;

namespace CapaNegocio
{
    ///Clase de acceso a Capa de DATOS - Capa de NEGOCIOS
    public class CN_Rotulados
    {
        ///Instancias de objeto de la capa de DATOS
        private CD_Rotulados objetoCD = new CD_Rotulados();

        ///Método de acceso a retorno del SP de tabla Tránsito para DataGridView
        public DataTable MostrarTra()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.MostrarTransito();
            return tabla;
        }

        ///Método de acceso a retorno de SP de Nombre de equipo para TextBox
        public DataTable NomEquipo()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.NombreEquipo();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Turno para ComboBox
        public DataTable ListarTur()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarTurno();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Lote para ComboBox
        public DataTable ListarLot()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarLote();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Corte para ComboBox
        public DataTable ListarCor()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarCorte();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Calidad para ComboBox
        public DataTable ListarCald()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarCalidad();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Conservación para ComboBox
        public DataTable ListarCons()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarConservacion();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Calibre para ComboBox
        public DataTable ListarCalib()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarCalibre();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Pieza para ComboBox
        public DataTable ListarPie()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarPieza();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Unidad para ComboBox
        public DataTable ListarUni()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarUnidad();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Especie para ComboBox
        public DataTable ListarEsp()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarEspecie();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Terminación para ComboBox
        public DataTable ListarTerm()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarTerminacion();
            return tabla;
        }

        ///Método de acceso a retorno de SP de tabla Etiqueta para ComboBox
        public DataTable ListarEti()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.ListarEtiqueta();
            return tabla;
        }

        ///Método de traspaso y conversión de parametros para Inserción mediante SP en tabla Tránsito
        public string InsertarTra(string equipo, string turno, string lote, string calibre, string especie, string corte, string terminacion, string calidad, string conservacion, string pieza, string unidad, string etiqueta, string kilos)
        {
            string mensaje;
            try
            {
                mensaje = objetoCD.InsertarTransito(
                        (equipo),
                        Convert.ToInt32(turno),
                        Convert.ToInt32(lote),
                        Convert.ToInt32(calibre),
                        Convert.ToInt32(especie),
                        Convert.ToInt32(corte),
                        Convert.ToInt32(terminacion),
                        Convert.ToInt32(calidad),
                        Convert.ToInt32(conservacion),
                        Convert.ToInt32(pieza),
                        Convert.ToInt32(unidad),
                        Convert.ToInt32(etiqueta),
                        Convert.ToDouble(kilos));
            }
            catch (Exception e)
            {
                e.ToString();
                mensaje = "2";
            }
            return mensaje;
        }

        ///Método de traspaso y conversión de parametros para activar FN de Cálculo de Kilos
        ///Retorna el resultado de la FN con los Kilos calculados
        public double IniciaFuncionCalculaKilos(string calibre, string pieza)
        {
            if (pieza == "") { pieza = "0"; };
            if (calibre == "") { calibre = "0"; };

            double kilo = objetoCD.ActivaFuncionCalculaKilos(
                                Convert.ToInt32(calibre),
                                Convert.ToInt32(pieza));
            return kilo;
        }

        ///Método de acceso a retorno de SP de código y etiqueta de tabla tránsito
        public DataTable TraeUltimoRegTrans()
        {
            DataTable tabla = new DataTable();
            tabla = objetoCD.TraeUltimoRegTransito();
            return tabla;
        }

        ///Método que envía los parámetros para llamar la etiqueta ed vuelta
        public String getEtiquetaTrans(string cod_transito, string cod_etiqueta)
        {
            CD_Rotulados objimprimir = new CD_Rotulados();
            String eti = objimprimir.getEtiquetaTransito(
                            Convert.ToInt32(cod_transito),
                            Convert.ToInt32(cod_etiqueta));
            return eti;
        }
    }
}
