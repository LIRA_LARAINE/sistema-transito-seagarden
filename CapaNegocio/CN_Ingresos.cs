﻿using CapaDatos;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Text;
using System.Threading.Tasks;

namespace CapaNegocio
{
    public  class CN_Ingresos
    {

        public static List<CD_Ingresos> GetOrigen() {


            List<CD_Ingresos> lista = CD_Ingresos.GetOrigen();

            return lista;

        
        }



        public static List<CD_Ingresos> GetCamara()
        {


            List<CD_Ingresos> lista = CD_Ingresos.GetCamara();

            return lista;


        }



        public static string GuardarIngreso(int id_transito, int id_sala, int id_camara)
        {

            String Resultado = "";
           
            CD_Ingresos ci = new CD_Ingresos();


            ci.id_transito = id_transito;
            ci.id_sala = id_sala;
            ci.id_camara = id_camara;

            Resultado = ci.GuardarIngreso(ci);


            return Resultado;


        }


        public static int GetIdEtiqueta (int id_transito)
        {

            int resultado = 0;

            CD_Ingresos ci = new CD_Ingresos();

            ci.id_transito = id_transito;

            resultado = ci.GetIdEtiqueta(ci);

            return resultado;
        }


        public static string GetEtiquetaZPL(int id_transito, int id_etiqueta)
        {

            String Resultado = "";

            CD_Ingresos ci = new CD_Ingresos();

            ci.id_transito = id_transito;
            ci.id_etiqueta = id_etiqueta;
            

            Resultado = ci.GetEtiquetaZPL(ci);

            return Resultado;


        }


    }
}
