﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public static class Utiles
    {


      



        public static bool IsNumeric(this string val)
        {
            float output;
            return float.TryParse(val, out output);
        }


        public static void seleccionar_texto(TextBox texto)
        {

            texto.SelectAll();
            texto.Focus();

        }



        public static void Etiqueta(string zpll)
        {

            //byte[] zpl = Encoding.UTF8.GetBytes("^xa^cfa,50^fo100,100^fdHello World^fs^xz");

            byte[] zpl = Encoding.UTF8.GetBytes(zpll);

            // adjust print density (8dpmm), label width (4 inches), label height (6 inches), and label index (0) as necessary
            var request = (HttpWebRequest)WebRequest.Create("http://api.labelary.com/v1/printers/8dpmm/labels/4x6/0/");
            request.Method = "POST";
            //request.Accept = "application/pdf"; // omit this line to get PNG images back
            request.ContentType = "application/x-www-form-urlencoded";
            request.Headers.Add("X-Rotation", "270");

            request.ContentLength = zpl.Length;

            var requestStream = request.GetRequestStream();
            requestStream.Write(zpl, 0, zpl.Length);
            requestStream.Close();

            try
            {




                var response = (HttpWebResponse)request.GetResponse();
                var responseStream = response.GetResponseStream();

               

                var fileStream = File.Create("label.png"); // change file name for PNG images
                responseStream.CopyTo(fileStream);
                responseStream.Close();
                fileStream.Close();
            }
            catch (WebException e)
            {
                Console.WriteLine("Error: {0}", e.Status);
            }



        }


    }
}
