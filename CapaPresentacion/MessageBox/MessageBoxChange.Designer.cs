﻿namespace CapaPresentacion
{
    partial class MessageBoxChange
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBoxChange));
            this.panelSuperiorMessageBoxChange = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panel5 = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.btnNoBoxChange = new System.Windows.Forms.Button();
            this.btnSiBoxChange = new System.Windows.Forms.Button();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.panelSuperiorMessageBoxChange.SuspendLayout();
            this.panel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panelSuperiorMessageBoxChange
            // 
            this.panelSuperiorMessageBoxChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panelSuperiorMessageBoxChange.Controls.Add(this.label2);
            this.panelSuperiorMessageBoxChange.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSuperiorMessageBoxChange.Location = new System.Drawing.Point(0, 0);
            this.panelSuperiorMessageBoxChange.Name = "panelSuperiorMessageBoxChange";
            this.panelSuperiorMessageBoxChange.Size = new System.Drawing.Size(320, 26);
            this.panelSuperiorMessageBoxChange.TabIndex = 0;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label2.Location = new System.Drawing.Point(7, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(70, 15);
            this.label2.TabIndex = 0;
            this.label2.Text = "Advertencia";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(6, 144);
            this.panel2.TabIndex = 1;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel3.Location = new System.Drawing.Point(314, 26);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(6, 144);
            this.panel3.TabIndex = 2;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel4.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel4.Location = new System.Drawing.Point(6, 164);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(308, 6);
            this.panel4.TabIndex = 3;
            // 
            // panel5
            // 
            this.panel5.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel5.Controls.Add(this.label1);
            this.panel5.Controls.Add(this.btnNoBoxChange);
            this.panel5.Controls.Add(this.btnSiBoxChange);
            this.panel5.Controls.Add(this.pictureBox1);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(6, 26);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(308, 138);
            this.panel5.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(69, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(229, 85);
            this.label1.TabIndex = 3;
            this.label1.Text = "Está intentando salir de esta sección, si no ha grabado el registro, los campos c" +
    "ompletados se perderán.\r\n\r\n¿Seguro que desea salir?";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnNoBoxChange
            // 
            this.btnNoBoxChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(15)))), ((int)(((byte)(6)))));
            this.btnNoBoxChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNoBoxChange.FlatAppearance.BorderSize = 0;
            this.btnNoBoxChange.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.btnNoBoxChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNoBoxChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoBoxChange.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnNoBoxChange.Location = new System.Drawing.Point(182, 106);
            this.btnNoBoxChange.Name = "btnNoBoxChange";
            this.btnNoBoxChange.Size = new System.Drawing.Size(60, 24);
            this.btnNoBoxChange.TabIndex = 2;
            this.btnNoBoxChange.Text = "No";
            this.btnNoBoxChange.UseVisualStyleBackColor = false;
            this.btnNoBoxChange.Click += new System.EventHandler(this.btnNoBoxChange_Click);
            // 
            // btnSiBoxChange
            // 
            this.btnSiBoxChange.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(15)))), ((int)(((byte)(6)))));
            this.btnSiBoxChange.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiBoxChange.FlatAppearance.BorderSize = 0;
            this.btnSiBoxChange.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.btnSiBoxChange.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiBoxChange.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiBoxChange.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiBoxChange.Location = new System.Drawing.Point(63, 106);
            this.btnSiBoxChange.Name = "btnSiBoxChange";
            this.btnSiBoxChange.Size = new System.Drawing.Size(60, 24);
            this.btnSiBoxChange.TabIndex = 1;
            this.btnSiBoxChange.Text = "Si";
            this.btnSiBoxChange.UseVisualStyleBackColor = false;
            this.btnSiBoxChange.Click += new System.EventHandler(this.btnSiBoxChange_Click);
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // MessageBoxChange
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 170);
            this.Controls.Add(this.panel5);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panelSuperiorMessageBoxChange);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MessageBoxChange";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MessageBoxChange";
            this.Load += new System.EventHandler(this.MessageBoxChange_Load);
            this.panelSuperiorMessageBoxChange.ResumeLayout(false);
            this.panelSuperiorMessageBoxChange.PerformLayout();
            this.panel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSuperiorMessageBoxChange;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSiBoxChange;
        private System.Windows.Forms.Button btnNoBoxChange;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
    }
}