﻿namespace CapaPresentacion
{
    partial class MessageBoxClose
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBoxClose));
            this.panelSuperiorMessageBoxClose = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnNoBoxClose = new System.Windows.Forms.Button();
            this.btnSiBoxClose = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.panelSuperiorMessageBoxClose.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.panel4.SuspendLayout();
            this.SuspendLayout();
            // 
            // panelSuperiorMessageBoxClose
            // 
            this.panelSuperiorMessageBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panelSuperiorMessageBoxClose.Controls.Add(this.label1);
            this.panelSuperiorMessageBoxClose.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSuperiorMessageBoxClose.Location = new System.Drawing.Point(0, 0);
            this.panelSuperiorMessageBoxClose.Name = "panelSuperiorMessageBoxClose";
            this.panelSuperiorMessageBoxClose.Size = new System.Drawing.Size(320, 26);
            this.panelSuperiorMessageBoxClose.TabIndex = 0;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(7, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(70, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Advertencia";
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 3;
            this.pictureBox1.TabStop = false;
            // 
            // btnNoBoxClose
            // 
            this.btnNoBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(15)))), ((int)(((byte)(6)))));
            this.btnNoBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnNoBoxClose.FlatAppearance.BorderSize = 0;
            this.btnNoBoxClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.btnNoBoxClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNoBoxClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnNoBoxClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnNoBoxClose.Location = new System.Drawing.Point(182, 106);
            this.btnNoBoxClose.Name = "btnNoBoxClose";
            this.btnNoBoxClose.Size = new System.Drawing.Size(60, 24);
            this.btnNoBoxClose.TabIndex = 2;
            this.btnNoBoxClose.Text = "No";
            this.btnNoBoxClose.UseVisualStyleBackColor = false;
            this.btnNoBoxClose.Click += new System.EventHandler(this.btnNoBoxClose_Click);
            // 
            // btnSiBoxClose
            // 
            this.btnSiBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(15)))), ((int)(((byte)(6)))));
            this.btnSiBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiBoxClose.FlatAppearance.BorderSize = 0;
            this.btnSiBoxClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.btnSiBoxClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiBoxClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiBoxClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiBoxClose.Location = new System.Drawing.Point(63, 106);
            this.btnSiBoxClose.Name = "btnSiBoxClose";
            this.btnSiBoxClose.Size = new System.Drawing.Size(60, 24);
            this.btnSiBoxClose.TabIndex = 1;
            this.btnSiBoxClose.Text = "Si";
            this.btnSiBoxClose.UseVisualStyleBackColor = false;
            this.btnSiBoxClose.Click += new System.EventHandler(this.btnSiBoxClose_Click);
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(70, 11);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(225, 76);
            this.label2.TabIndex = 0;
            this.label2.Text = "Está intentando salir del programa.\r\n\r\n¿Seguro que desea salir?";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(314, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(6, 144);
            this.panel1.TabIndex = 2;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(6, 144);
            this.panel2.TabIndex = 3;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(6, 164);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(308, 6);
            this.panel3.TabIndex = 4;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.btnSiBoxClose);
            this.panel4.Controls.Add(this.btnNoBoxClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(6, 26);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(308, 138);
            this.panel4.TabIndex = 5;
            // 
            // MessageBoxClose
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(320, 170);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelSuperiorMessageBoxClose);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MessageBoxClose";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Mensaje del Sistema";
            this.Load += new System.EventHandler(this.MessageBoxClose_Load);
            this.panelSuperiorMessageBoxClose.ResumeLayout(false);
            this.panelSuperiorMessageBoxClose.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.panel4.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panelSuperiorMessageBoxClose;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnSiBoxClose;
        private System.Windows.Forms.Button btnNoBoxClose;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel4;
    }
}