﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class MessageBoxChange : Form
    {
        public MessageBoxChange()
        {
            InitializeComponent();
        }

        private void MessageBoxChange_Load(object sender, EventArgs e)
        {

        }

        ///Método que instancia la ventana de mensaje auxiliar
        private void AbrirInicio(object Mess)
        {
            Form Msj = Mess as Form;
            Msj.ShowDialog();
        }

        ///Redirije hacia el menu principal
        private void btnSiBoxChange_Click(object sender, EventArgs e)
        {
            this.Close();
            AbrirInicio(new Inicio());
        }

        ///Cierra ventana
        private void btnNoBoxChange_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
