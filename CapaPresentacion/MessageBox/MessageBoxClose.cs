﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    ///Formulario de que cierra la aplicación
    public partial class MessageBoxClose : Form
    {
        public MessageBoxClose()
        {
            InitializeComponent();
        }

        private void MessageBoxClose_Load(object sender, EventArgs e)
        {

        }

        ///Cierra aplicación
        private void btnSiBoxClose_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
        
        ///Cierra ventana
        private void btnNoBoxClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
