﻿namespace CapaPresentacion.MessageBox
{
    partial class MessageBoxInsertNO
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MessageBoxInsertNO));
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panelSuperiorMessageBoxInsertOK = new System.Windows.Forms.Panel();
            this.label1 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label2 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnSiBoxClose = new System.Windows.Forms.Button();
            this.panelSuperiorMessageBoxInsertOK.SuspendLayout();
            this.panel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel3.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.panel3.Location = new System.Drawing.Point(6, 164);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(308, 6);
            this.panel3.TabIndex = 12;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel2.Dock = System.Windows.Forms.DockStyle.Left;
            this.panel2.Location = new System.Drawing.Point(0, 26);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(6, 144);
            this.panel2.TabIndex = 11;
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(314, 26);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(6, 144);
            this.panel1.TabIndex = 10;
            // 
            // panelSuperiorMessageBoxInsertOK
            // 
            this.panelSuperiorMessageBoxInsertOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.panelSuperiorMessageBoxInsertOK.Controls.Add(this.label1);
            this.panelSuperiorMessageBoxInsertOK.Dock = System.Windows.Forms.DockStyle.Top;
            this.panelSuperiorMessageBoxInsertOK.Location = new System.Drawing.Point(0, 0);
            this.panelSuperiorMessageBoxInsertOK.Name = "panelSuperiorMessageBoxInsertOK";
            this.panelSuperiorMessageBoxInsertOK.Size = new System.Drawing.Size(320, 26);
            this.panelSuperiorMessageBoxInsertOK.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.label1.Location = new System.Drawing.Point(7, 5);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(123, 15);
            this.label1.TabIndex = 0;
            this.label1.Text = "Mensaje del Sistema";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.panel4.Controls.Add(this.label2);
            this.panel4.Controls.Add(this.pictureBox1);
            this.panel4.Controls.Add(this.btnSiBoxClose);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(6, 26);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(308, 138);
            this.panel4.TabIndex = 13;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(65, 5);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(237, 93);
            this.label2.TabIndex = 8;
            this.label2.Text = "Error: No fue posible grabar el registro, recuerde que:\r\n\r\nNo se permiten campos " +
    "en blanco.\r\nDebe ingresar registros válidos.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
            this.pictureBox1.Location = new System.Drawing.Point(11, 19);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(50, 60);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBox1.TabIndex = 10;
            this.pictureBox1.TabStop = false;
            // 
            // btnSiBoxClose
            // 
            this.btnSiBoxClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(56)))), ((int)(((byte)(15)))), ((int)(((byte)(6)))));
            this.btnSiBoxClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnSiBoxClose.FlatAppearance.BorderSize = 0;
            this.btnSiBoxClose.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(148)))), ((int)(((byte)(28)))), ((int)(((byte)(1)))));
            this.btnSiBoxClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSiBoxClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSiBoxClose.ForeColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnSiBoxClose.Location = new System.Drawing.Point(114, 106);
            this.btnSiBoxClose.Name = "btnSiBoxClose";
            this.btnSiBoxClose.Size = new System.Drawing.Size(76, 24);
            this.btnSiBoxClose.TabIndex = 9;
            this.btnSiBoxClose.Text = "Aceptar";
            this.btnSiBoxClose.UseVisualStyleBackColor = false;
            this.btnSiBoxClose.Click += new System.EventHandler(this.btnSiBoxClose_Click);
            // 
            // MessageBoxInsertNO
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(320, 170);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.panelSuperiorMessageBoxInsertOK);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "MessageBoxInsertNO";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MessageBoxInsertNO";
            this.Load += new System.EventHandler(this.MessageBoxInsertNO_Load);
            this.panelSuperiorMessageBoxInsertOK.ResumeLayout(false);
            this.panelSuperiorMessageBoxInsertOK.PerformLayout();
            this.panel4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelSuperiorMessageBoxInsertOK;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnSiBoxClose;
    }
}