﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;
using CapaPresentacion.MessageBox;

namespace CapaPresentacion
{
    ///Clase de acceso a Capa de NEGOCIOS - Capa de PRESENTACIÓN
    public partial class FormRotulado : Form
    {
        ///Instancias de objeto de la capa de DATOS e instanciaciones
        CN_Rotulados objetoCN = new CN_Rotulados();
        AutoCompleteStringCollection coleccion;

        public FormRotulado()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            MostrarTransito();
            NombreEquipo();
            ListarTurnos();
            ListarLotes();
            ListarCortes();
            ListarCalidades();
            ListarConservaciones();
            ListarCalibres();
            ListarPiezas();
            ListarUnidades();
            ListarEspecie();
            ListarTerminacion();
            ListarEtiqueta();
        }

        ///Método que instancia la ventana de sistema para confirmar el cambio de sección
        private void AbrirMessageBox(object Mess)
        {
            Form Msj = Mess as Form;
            Msj.ShowDialog();
        }

        ///Método para mostrar el retorno del SP de tabla Tránsito en DataGridView
        private void MostrarTransito()
        {
            CN_Rotulados objeto0 = new CN_Rotulados();
            dataGridViewRotulados.DataSource = objeto0.MostrarTra();
        }

        ///Método para mostrar el retorno del SP de nombre de equipo en TextBox
        public void NombreEquipo()
        {
            CN_Rotulados obje = new CN_Rotulados();
            foreach (DataRow reg in obje.NomEquipo().Rows)
            {
                textEquipo.Text = reg["nombre"].ToString();
            }
        }

        ///Método para mostrar el retorno del SP de tabla Turno en ComboBox
        private void ListarTurnos()
        {
            CN_Rotulados objeto1 = new CN_Rotulados();
            cbTurno.DataSource = objeto1.ListarTur();
            cbTurno.DisplayMember = "NomTurno";
            cbTurno.ValueMember = "codTurno";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto1.ListarTur().Rows)
            {
                coleccion.Add(Convert.ToString(row["NomTurno"]));
            }
            cbTurno.AutoCompleteCustomSource = coleccion;
            cbTurno.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbTurno.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbTurno.SelectedIndex = -1;

        }

        ///Método para mostrar el retorno del SP de tabla Lote en ComboBox
        private void ListarLotes()
        {
            CN_Rotulados objeto2 = new CN_Rotulados();
            cbLote.DataSource = objeto2.ListarLot();
            cbLote.DisplayMember = "nombre";
            cbLote.ValueMember = "cod_lote";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto2.ListarLot().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbLote.AutoCompleteCustomSource = coleccion;
            cbLote.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbLote.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbLote.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Corte en ComboBox
        private void ListarCortes()
        {
            CN_Rotulados objeto3 = new CN_Rotulados();
            cbCorte.DataSource = objeto3.ListarCor();
            cbCorte.DisplayMember = "nombre";
            cbCorte.ValueMember = "cod_corte";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto3.ListarCor().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbCorte.AutoCompleteCustomSource = coleccion;
            cbCorte.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbCorte.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbCorte.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Calidad en ComboBox
        private void ListarCalidades()
        {
            CN_Rotulados objeto4 = new CN_Rotulados();
            cbCalidad.DataSource = objeto4.ListarCald();
            cbCalidad.DisplayMember = "nombre";
            cbCalidad.ValueMember = "cod_cald";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto4.ListarCald().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbCalidad.AutoCompleteCustomSource = coleccion;
            cbCalidad.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbCalidad.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbCalidad.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Conservación en ComboBox
        private void ListarConservaciones()
        {
            CN_Rotulados objeto5 = new CN_Rotulados();
            cbConservacion.DataSource = objeto5.ListarCons();
            cbConservacion.DisplayMember = "nombre";
            cbConservacion.ValueMember = "cod_cons";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto5.ListarCons().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbConservacion.AutoCompleteCustomSource = coleccion;
            cbConservacion.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbConservacion.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbConservacion.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Calibre en ComboBox
        private void ListarCalibres()
        {
            CN_Rotulados objeto6 = new CN_Rotulados();
            cbCalibre.DataSource = objeto6.ListarCalib();
            cbCalibre.DisplayMember = "nombre";
            cbCalibre.ValueMember = "cod_calib";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto6.ListarCalib().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbCalibre.AutoCompleteCustomSource = coleccion;
            cbCalibre.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbCalibre.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbCalibre.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Pieza en ComboBox
        private void ListarPiezas()
        {
            CN_Rotulados objeto7 = new CN_Rotulados();
            cbPieza.DataSource = objeto7.ListarPie();
            cbPieza.DisplayMember = "codigo";
            cbPieza.ValueMember = "cod_pieza";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto7.ListarPie().Rows)
            {
                coleccion.Add(Convert.ToString(row["codigo"]));
            }
            cbPieza.AutoCompleteCustomSource = coleccion;
            cbPieza.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbPieza.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbPieza.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Unidad en ComboBox
        private void ListarUnidades()
        {
            CN_Rotulados objeto8 = new CN_Rotulados();
            cbUnidad.DataSource = objeto8.ListarUni();
            cbUnidad.DisplayMember = "nombre";
            cbUnidad.ValueMember = "cod_uni";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto8.ListarUni().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbUnidad.AutoCompleteCustomSource = coleccion;
            cbUnidad.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbUnidad.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbUnidad.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Especie en ComboBox
        private void ListarEspecie()
        {
            CN_Rotulados objeto9 = new CN_Rotulados();
            cbEspecie.DataSource = objeto9.ListarEsp();
            cbEspecie.DisplayMember = "descripcion";
            cbEspecie.ValueMember = "cod_especie";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto9.ListarEsp().Rows)
            {
                coleccion.Add(Convert.ToString(row["descripcion"]));
            }
            cbEspecie.AutoCompleteCustomSource = coleccion;
            cbEspecie.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbEspecie.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbEspecie.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Terminación en ComboBox
        private void ListarTerminacion()
        {
            CN_Rotulados objeto10 = new CN_Rotulados();
            cbTerminacion.DataSource = objeto10.ListarTerm();
            cbTerminacion.DisplayMember = "nombre";
            cbTerminacion.ValueMember = "cod_term";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto10.ListarTerm().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbTerminacion.AutoCompleteCustomSource = coleccion;
            cbTerminacion.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbTerminacion.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbTerminacion.SelectedIndex = -1;
        }

        ///Método para mostrar el retorno del SP de tabla Etiqueta en ComboBox
        private void ListarEtiqueta()
        {
            CN_Rotulados objeto11 = new CN_Rotulados();
            cbEtiqueta.DataSource = objeto11.ListarEti();
            cbEtiqueta.DisplayMember = "nombre";
            cbEtiqueta.ValueMember = "cod_meti";

            ///Se instancia para autocompletar la búsqueda del ComboBox
            coleccion = new AutoCompleteStringCollection();
            foreach (DataRow row in objeto11.ListarEti().Rows)
            {
                coleccion.Add(Convert.ToString(row["nombre"]));
            }
            cbEtiqueta.AutoCompleteCustomSource = coleccion;
            cbEtiqueta.AutoCompleteMode = AutoCompleteMode.SuggestAppend;
            cbEtiqueta.AutoCompleteSource = AutoCompleteSource.CustomSource;
            cbEtiqueta.SelectedIndex = -1;
        }

        ///Método de botón Agregar para enviar parámetros de inserción para SP en tabla Tránsito
        private void btnInsertar_Click(object sender, EventArgs e)
        {
            ///Limpiar errorprovider
            ErrorAlerta.Clear();

            ///Ciclo para revisar nulos de combobox y activar errorprovider
            var controls = new[] { cbTurno, cbLote, cbEspecie, cbTerminacion, cbCorte, cbCalidad, cbConservacion, cbUnidad, cbEtiqueta, cbCalibre, cbPieza };
            foreach (var control in controls.Where(i => String.IsNullOrEmpty(i.Text)))
            {
                ErrorAlerta.SetError(control, "Seleccione o ingrese un dato válido");
            }

            ///Envía datos capturados desde el formulario a capa de negocio para inserción de rotulado
            string mensaje = objetoCN.InsertarTra(
                            (textEquipo.Text),
                            Convert.ToString(cbTurno.SelectedValue),
                            Convert.ToString(cbLote.SelectedValue),
                            Convert.ToString(cbCalibre.SelectedValue),
                            Convert.ToString(cbEspecie.SelectedValue),
                            Convert.ToString(cbCorte.SelectedValue),
                            Convert.ToString(cbTerminacion.SelectedValue),
                            Convert.ToString(cbCalidad.SelectedValue),
                            Convert.ToString(cbConservacion.SelectedValue),
                            Convert.ToString(cbPieza.SelectedValue),
                            Convert.ToString(cbUnidad.SelectedValue),
                            Convert.ToString(cbEtiqueta.SelectedValue),
                            textKilos.Text);

            ///Captura mensaje desde base de datos o error de selección de combobox y llama a MessageBox
            if (mensaje == "1")
            {
                AbrirMessageBox(new MessageBoxInsertOK());
                MostrarTransito();
                ImprimirEtiqueta();
            }
            else
            {
                if (mensaje == "2")
                {
                    AbrirMessageBox(new MessageBoxInsertNO());
                    MostrarTransito();
                }
            }
            return;
        }

        ///Métodos que activa el evento SelectionChangeCommited del ComboBox Calibre
        private void Variable_Calibre(object sender, EventArgs e)
        {
            Object SelectedValue = Convert.ToString(cbCalibre.SelectedValue);
            Captura_variables();
        }

        ///Métodos que activa el evento SelectionChangeCommited del ComboBox Pieza
        private void Variable_Pieza(object sender, EventArgs e)
        {
            Object SelectedValue = Convert.ToString(cbPieza.SelectedValue);
            Captura_variables();
        }

        ///Método que captura los SelectValue de SelectionChangeCommited de los ComboBox Calibre y Pieza
        ///para enviarlos como parámetro a la FN que calculará los Kilos
        public double Captura_variables()
        {
            double kilo = objetoCN.IniciaFuncionCalculaKilos(
                                Convert.ToString(cbCalibre.SelectedValue),
                                Convert.ToString(cbPieza.SelectedValue));

            ///Asigna el valor de retorno de la FN al TextBox Kilos
            if (kilo != 0)
            {
                textKilos.Text = kilo.ToString();
            }
            return kilo;
        }

        ///Método que trae el último código ingresado en la tabla tránsito
        private string TraeUltimoRegTransito()
        {
            string id_transito = "";
            CN_Rotulados objereg = new CN_Rotulados();
            foreach (DataRow reg in objereg.TraeUltimoRegTrans().Rows)
            {
                id_transito = reg["cod_transito"].ToString();
            }
            return id_transito;
        }

        ///Método que trae la última etiqueta ingresado en la tabla tránsito
        private string TraeUltimoRegEtiqueta()
        {
            string id_etiqueta = "";
            CN_Rotulados objereg = new CN_Rotulados();
            foreach (DataRow reg in objereg.TraeUltimoRegTrans().Rows)
            {
                id_etiqueta = reg["id_etiqueta"].ToString();
            }
            return id_etiqueta;
        }

        ///Método que crea etiqueta de tránsito enviando los parametros del último registro
        private void ImprimirEtiqueta()
        {
            CN_Rotulados objeto12 = new CN_Rotulados();
            string eti = objeto12.getEtiquetaTrans(
                            TraeUltimoRegTransito(),
                            TraeUltimoRegEtiqueta());
            string path = Directory.GetCurrentDirectory() + "\\etiqueta.txt";
            using (StreamWriter writer = File.CreateText(path))
            {
                writer.WriteLine(eti);
            }
            File.Copy(path, "LPT1", true);
        }

        private void btnLimpiar_Click(object sender, EventArgs e)
        {
            ErrorAlerta.Clear();
            textKilos.Text = "";
            var controls = new[] { cbTurno, cbLote, cbEspecie, cbTerminacion, cbCorte, cbCalidad, cbConservacion, cbUnidad, cbEtiqueta, cbCalibre, cbPieza };
            foreach (var control in controls)
            {
                control.Text = null;
            }
        }
    }
}
