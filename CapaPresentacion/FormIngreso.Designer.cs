﻿namespace CapaPresentacion
{
    partial class FormIngreso
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label11 = new System.Windows.Forms.Label();
            this.CbSalaOrigen = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.CbCamara = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.txt_barra = new System.Windows.Forms.TextBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.ImagenZPL = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenZPL)).BeginInit();
            this.SuspendLayout();
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(306, 25);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(190, 24);
            this.label11.TabIndex = 24;
            this.label11.Text = "Ingreso de Productos";
            // 
            // CbSalaOrigen
            // 
            this.CbSalaOrigen.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbSalaOrigen.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CbSalaOrigen.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbSalaOrigen.FormattingEnabled = true;
            this.CbSalaOrigen.Location = new System.Drawing.Point(152, 91);
            this.CbSalaOrigen.Name = "CbSalaOrigen";
            this.CbSalaOrigen.Size = new System.Drawing.Size(176, 28);
            this.CbSalaOrigen.TabIndex = 25;
            this.CbSalaOrigen.TabStop = false;
            this.CbSalaOrigen.SelectedValueChanged += new System.EventHandler(this.CbSalaOrigen_SelectedValueChanged);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(46, 94);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(92, 20);
            this.label8.TabIndex = 26;
            this.label8.Text = "Sala Origen";
            // 
            // CbCamara
            // 
            this.CbCamara.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbCamara.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CbCamara.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbCamara.FormattingEnabled = true;
            this.CbCamara.Location = new System.Drawing.Point(152, 162);
            this.CbCamara.Name = "CbCamara";
            this.CbCamara.Size = new System.Drawing.Size(176, 28);
            this.CbCamara.TabIndex = 27;
            this.CbCamara.TabStop = false;
            this.CbCamara.SelectedValueChanged += new System.EventHandler(this.CbCamara_SelectedValueChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(46, 165);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 20);
            this.label1.TabIndex = 28;
            this.label1.Text = "Camara";
            // 
            // txt_barra
            // 
            this.txt_barra.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_barra.Location = new System.Drawing.Point(353, 91);
            this.txt_barra.Name = "txt_barra";
            this.txt_barra.Size = new System.Drawing.Size(434, 98);
            this.txt_barra.TabIndex = 0;
            this.txt_barra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_barra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_barra_KeyPress);
            // 
            // lblMensaje
            // 
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ForeColor = System.Drawing.Color.Chartreuse;
            this.lblMensaje.Location = new System.Drawing.Point(43, 193);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(744, 44);
            this.lblMensaje.TabIndex = 29;
            this.lblMensaje.Text = "-";
            this.lblMensaje.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // ImagenZPL
            // 
            this.ImagenZPL.Location = new System.Drawing.Point(50, 240);
            this.ImagenZPL.Name = "ImagenZPL";
            this.ImagenZPL.Size = new System.Drawing.Size(737, 263);
            this.ImagenZPL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImagenZPL.TabIndex = 30;
            this.ImagenZPL.TabStop = false;
            // 
            // FormIngreso
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(36)))), ((int)(((byte)(34)))));
            this.ClientSize = new System.Drawing.Size(812, 515);
            this.Controls.Add(this.ImagenZPL);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.txt_barra);
            this.Controls.Add(this.CbCamara);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.CbSalaOrigen);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label11);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormIngreso";
            this.Text = "FormIngreso";
            this.Load += new System.EventHandler(this.FormIngreso_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImagenZPL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.ComboBox CbSalaOrigen;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox CbCamara;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txt_barra;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.PictureBox ImagenZPL;
    }
}