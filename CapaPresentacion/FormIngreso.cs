﻿
using CapaNegocio;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FormIngreso : Form
    {
        public FormIngreso()
        {
            InitializeComponent();
        }

        private void FormIngreso_Load(object sender, EventArgs e)
        {


            CbSalaOrigen.DataSource = CN_Ingresos.GetOrigen();
            CbSalaOrigen.DisplayMember = "nombre_sala";
            CbSalaOrigen.ValueMember = "id_sala";


            CbCamara.DataSource = CN_Ingresos.GetCamara();
            CbCamara.DisplayMember = "nombre_camara";
            CbCamara.ValueMember = "id_Camara";


        }



       


        private void txt_barra_KeyPress(object sender, KeyPressEventArgs e)
        
        {

            lblMensaje.Text = "";


            if (e.KeyChar == (char)Keys.Enter)
            {



                
               
                ImagenZPL.Image = null;
                ImagenZPL.Update();

                bool ValidaNumero = Utiles.IsNumeric(txt_barra.Text);
                                
                if (ValidaNumero == false) { lblMensaje.Text = "Debe Ingresar Numero"; Utiles.seleccionar_texto(txt_barra);  return; }

                Int32 id_transito = Convert.ToInt32(txt_barra.Text);
                Int32 id_sala = Convert.ToInt32(CbSalaOrigen.SelectedValue);
                Int32 id_camara = Convert.ToInt32(CbCamara.SelectedValue);
                

                string Mensaje = CN_Ingresos.GuardarIngreso(id_transito, id_sala,id_camara);

                lblMensaje.Text = Mensaje;

                Utiles.seleccionar_texto(txt_barra);


                if ((Mensaje == "Registro Ingresado") || (Mensaje == "Registro Existe en Stock"))  {


                    Int32 id_etiqueta = CN_Ingresos.GetIdEtiqueta(id_transito);

                    string zpl = CN_Ingresos.GetEtiquetaZPL(id_transito, id_etiqueta);

                                    
                    Utiles.Etiqueta(zpl);


                    using (StreamReader stream = new StreamReader("label.png"))
                    {
                        ImagenZPL.Image = Image.FromStream(stream.BaseStream);
                    }

                }



            }




        }

        private void CbSalaOrigen_SelectedValueChanged(object sender, EventArgs e)
        {
            Utiles.seleccionar_texto(txt_barra);


        }

        private void CbCamara_SelectedValueChanged(object sender, EventArgs e)
        {
            Utiles.seleccionar_texto(txt_barra);
        }
    }
}
