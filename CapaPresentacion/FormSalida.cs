﻿using CapaNegocio;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CapaPresentacion
{
    public partial class FormSalida : Form
    {
        public FormSalida()
        {
            InitializeComponent();
        }

        private void txt_barra_KeyPress(object sender, KeyPressEventArgs e)
        {


            lblMensaje.Text = "";


            if (e.KeyChar == (char)Keys.Enter)
            {





                ImagenZPL.Image = null;
                ImagenZPL.Update();

                bool ValidaNumero = Utiles.IsNumeric(txt_barra.Text);

                if (ValidaNumero == false) { lblMensaje.Text = "Debe Ingresar Numero"; Utiles.seleccionar_texto(txt_barra); return; }

                Int32 id_transito = Convert.ToInt32(txt_barra.Text);
                Int32 id_motivo = Convert.ToInt32(CbMotivo.SelectedValue);



                //string Mensaje = "";
                //CN_Ingresos.GuardarIngreso(id_transito, id_sala, id_camara);

                string Mensaje=CN_Salidas.GuardarSalida(id_transito, id_motivo);

                lblMensaje.Text = Mensaje;

                Utiles.seleccionar_texto(txt_barra);


                if ((Mensaje == "Registro Egresado") || (Mensaje == "Etiqueta Registra Egreso"))
                {


                    Int32 id_etiqueta = CN_Ingresos.GetIdEtiqueta(id_transito);

                    string zpl = CN_Ingresos.GetEtiquetaZPL(id_transito, id_etiqueta);


                    Utiles.Etiqueta(zpl);


                    using (StreamReader stream = new StreamReader("label.png"))
                    {
                        ImagenZPL.Image = Image.FromStream(stream.BaseStream);
                    }

                }



            }

        }

        private void FormSalida_Load(object sender, EventArgs e)
        {



            CbMotivo.DataSource = CN_Salidas.GetMotivo();
            CbMotivo.DisplayMember = "nombre_motivo";
            CbMotivo.ValueMember = "id_motivo";




        }
    }
}
