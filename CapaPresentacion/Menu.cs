﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Runtime.InteropServices;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using CapaNegocio;

namespace CapaPresentacion
{
    ///Formulario de Menú Principal
    public partial class Menu : Form
    {

        public Menu()
        {
            InitializeComponent();
            this.Text = string.Empty;
            this.ControlBox = false;
            this.DoubleBuffered = true;
            this.MaximizedBounds = Screen.FromHandle(this.Handle).WorkingArea;
        }

        ///Constructor
        private void Menu_Load(object sender, EventArgs e)
        {

        }

        ///Método para maximizar dentro del rango de la pantalla
        int LX, LY, SW, SH;
        private void pictureBoxMaximizar_Click(object sender, EventArgs e)
        {
            LX = this.Location.X;
            LY = this.Location.Y;
            SW = this.Size.Width;
            SH = this.Size.Height;
            this.Size = Screen.PrimaryScreen.WorkingArea.Size;
            this.Location = Screen.PrimaryScreen.WorkingArea.Location;
            pictureBoxMaximizar.Visible = false;
            pictureBoxRestaurar.Visible = true;
        }

        ///Método para restaurar el tamaño de la aplicación
        private void pictureBoxRestaurar_Click(object sender, EventArgs e)
        {
            this.Size = new Size(SW, SH);
            this.Location = new Point(LX, LY);
            pictureBoxRestaurar.Visible = false;
            pictureBoxMaximizar.Visible = true;
        }

        ///Método para minimizar la aplicación
        private void pictureBoxMinimizar_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        ///Importación de librerías para mover la aplicación desde el panel superior
        [DllImport("user32.DLL", EntryPoint = "ReleaseCapture")]
        private extern static void ReleaseCapture();
        [DllImport("user32.DLL", EntryPoint = "SendMessage")]
        private extern static void SendMessage(System.IntPtr hWnd, int wMsg, int wParam, int lParam);

        ///Acción de arrastrar el panel con el mouse
        private void panelBarra_MouseDown(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(this.Handle, 0x112, 0xf012, 0);
        }

        ///Método que instancia la ventana de sistema para confirmar el cierre de la aplicación
        private void AbrirMessageBoxClose(object Mess)
        {
            Form Msj = Mess as Form;
            Msj.ShowDialog();
        }

        ///Método que invoca al form para cerrar aplicación
        private void pictureBoxCerrar_Click(object sender, EventArgs e)
        {
            AbrirMessageBoxClose(new MessageBoxClose());
        }

        ///Método que configura la apertura de un form dentro del panel Central del form principal (Menú)
        private void AbrirFormHijo(object formhijo)
        {
            if (this.panelCentral.Controls.Count > 0)
            {
                this.panelCentral.Controls.RemoveAt(0);
                Form fh = formhijo as Form;
                fh.TopLevel = false;
                fh.Dock = DockStyle.Fill;
                this.panelCentral.Controls.Add(fh);
                this.panelCentral.Tag = fh;
                fh.Show();
            }
        }

        ///Mostrar hora y fecha de sistema
        private void HoraFecha_Tick(object sender, EventArgs e)
        {
            labelHora.Text = DateTime.Now.ToString("HH:mm:ss");
            labelFecha.Text = DateTime.Now.ToLongDateString();
        }

        ///Método que invoca al form para abrir el form Rotulados
        private void btnRotulados_Click(object sender, EventArgs e)
        {
            AbrirFormHijo(new FormRotulado());
        }

        ///Método que vuelve al menú principal desde el ícono
        private void BtnInicio_Click(object sender, EventArgs e)
        {
            AbrirFormHijo(new Inicio());
        }
        private void btnIngresos_Click(object sender, EventArgs e)
        {
            AbrirFormHijo(new FormIngreso());
        }

        private void btnSalidas_Click(object sender, EventArgs e)
        {
            AbrirFormHijo(new FormSalida());
        }
    }
    
}
