﻿namespace CapaPresentacion
{
    partial class FormSalida
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ImagenZPL = new System.Windows.Forms.PictureBox();
            this.lblMensaje = new System.Windows.Forms.Label();
            this.txt_barra = new System.Windows.Forms.TextBox();
            this.CbMotivo = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.ImagenZPL)).BeginInit();
            this.SuspendLayout();
            // 
            // ImagenZPL
            // 
            this.ImagenZPL.Location = new System.Drawing.Point(33, 201);
            this.ImagenZPL.Name = "ImagenZPL";
            this.ImagenZPL.Size = new System.Drawing.Size(737, 263);
            this.ImagenZPL.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.ImagenZPL.TabIndex = 36;
            this.ImagenZPL.TabStop = false;
            // 
            // lblMensaje
            // 
            this.lblMensaje.Font = new System.Drawing.Font("Microsoft Sans Serif", 26.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMensaje.ForeColor = System.Drawing.Color.Chartreuse;
            this.lblMensaje.Location = new System.Drawing.Point(26, 151);
            this.lblMensaje.Name = "lblMensaje";
            this.lblMensaje.Size = new System.Drawing.Size(744, 44);
            this.lblMensaje.TabIndex = 35;
            this.lblMensaje.Text = "-";
            this.lblMensaje.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txt_barra
            // 
            this.txt_barra.Font = new System.Drawing.Font("Microsoft Sans Serif", 60F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txt_barra.Location = new System.Drawing.Point(336, 50);
            this.txt_barra.Name = "txt_barra";
            this.txt_barra.Size = new System.Drawing.Size(434, 98);
            this.txt_barra.TabIndex = 31;
            this.txt_barra.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.txt_barra.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_barra_KeyPress);
            // 
            // CbMotivo
            // 
            this.CbMotivo.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.CbMotivo.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CbMotivo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CbMotivo.FormattingEnabled = true;
            this.CbMotivo.Location = new System.Drawing.Point(143, 50);
            this.CbMotivo.Name = "CbMotivo";
            this.CbMotivo.Size = new System.Drawing.Size(176, 28);
            this.CbMotivo.TabIndex = 33;
            this.CbMotivo.TabStop = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.ForeColor = System.Drawing.Color.White;
            this.label8.Location = new System.Drawing.Point(29, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(103, 20);
            this.label8.TabIndex = 34;
            this.label8.Text = "Motivo Salida";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.White;
            this.label11.Location = new System.Drawing.Point(289, -1);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(178, 24);
            this.label11.TabIndex = 32;
            this.label11.Text = "Salida de Productos";
            // 
            // FormSalida
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoValidate = System.Windows.Forms.AutoValidate.EnablePreventFocusChange;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(46)))), ((int)(((byte)(36)))), ((int)(((byte)(34)))));
            this.ClientSize = new System.Drawing.Size(796, 476);
            this.Controls.Add(this.ImagenZPL);
            this.Controls.Add(this.lblMensaje);
            this.Controls.Add(this.txt_barra);
            this.Controls.Add(this.CbMotivo);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label11);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormSalida";
            this.Text = "FormEgreso";
            this.Load += new System.EventHandler(this.FormSalida_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ImagenZPL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox ImagenZPL;
        private System.Windows.Forms.Label lblMensaje;
        private System.Windows.Forms.TextBox txt_barra;
        private System.Windows.Forms.ComboBox CbMotivo;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label11;
    }
}