USE [bdsystem]
GO

/****** Object:  StoredProcedure [dbo].[SP_CamposEtiquetaTRN]    Script Date: 05/08/2020 17:43:26 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

-- =============================================
-- Author:		Marcos Campos 
-- Create date: 04/08/2020
-- Description:	Campos Impresion Etiqueta Transito
-- =============================================
CREATE OR ALTER PROCEDURE  [dbo].[SP_CamposEtiquetaTRN]
(
@IdCaja int
)

AS
BEGIN
	
SELECT        dbo.transito.cod_transito As IdCaja, dbo.especies.descripcion AS N_Especie, dbo.empresas.descripcion AS N_Empresa, dbo.turno.NomTurno AS N_Turno, dbo.lotes.nombre AS N_Lote, Format(CONVERT(datetime, dbo.lotes.reg_cosecha), 'dd-MM-yyyy') 
                         AS N_FechaProd, dbo.calibre.nombre AS N_Calibre, dbo.corte.nombre AS N_Corte, dbo.terminacion.nombre AS N_Terminacion, dbo.calidad.nombre AS N_Calidad, dbo.conservacion.nombre AS N_Conservacion, 
                         dbo.unidad.nombre AS N_Unidad, dbo.pieza.codigo AS N_Piezas, dbo.transito.kilos AS N_Kilos, 0 As N_Usuario
FROM            dbo.transito INNER JOIN
                         dbo.turno ON dbo.transito.id_turno = dbo.turno.CodTurno INNER JOIN
                         dbo.corte ON dbo.transito.id_corte = dbo.corte.cod_corte INNER JOIN
                         dbo.calidad ON dbo.transito.id_calidad = dbo.calidad.cod_cald LEFT OUTER JOIN
                         dbo.pieza ON dbo.transito.id_pieza = dbo.pieza.cod_pieza LEFT OUTER JOIN
                         dbo.unidad ON dbo.transito.id_unidad = dbo.unidad.cod_uni LEFT OUTER JOIN
                         dbo.conservacion ON dbo.transito.id_conservacion = dbo.conservacion.cod_cons LEFT OUTER JOIN
                         dbo.terminacion ON dbo.transito.id_terminacion = dbo.terminacion.cod_term LEFT OUTER JOIN
                         dbo.lotes ON dbo.transito.id_lote = dbo.lotes.cod_lote LEFT OUTER JOIN
                         dbo.calibre ON dbo.transito.id_calibre = dbo.calibre.cod_calib LEFT OUTER JOIN
                         dbo.empresas ON dbo.lotes.cod_empresa = dbo.empresas.cod_empresa LEFT OUTER JOIN
                         dbo.especies ON dbo.transito.id_especie = dbo.especies.cod_especie
WHERE        (dbo.transito.cod_transito = @IdCaja)


END
GO


CREATE OR ALTER PROCEDURE [dbo].[CrearEtiquetaTRN]
(
@IdCaja int,
@IdEtiqueta as int

)

AS
BEGIN
Declare 
@str_text as varchar(1000),
@empresa as varchar(100),
@especie as varchar(50),
@corte as varchar(50),
@conservacion as varchar(50),
@terminacion as varchar(50),
@calidad as varchar(50),
@calibre as varchar(50),
@Unidad as varchar(50),
@lote as varchar(50),
@fechaprod as varchar(10),
@kilos as varchar(6),
@piezas as varchar(3),
@turno as varchar(50),
@usuario as varchar(50),
@fechaimpresion as datetime,
@str_linea as nvarchar(MAX)


DECLARE @TablaTemporal TABLE (idlinea int, texto varchar(1000))

Declare @TablaEtiqueta TABLE(IdCaja int PRIMARY KEY not null, c_especie varchar(50), c_empresa varchar(100),
c_turno varchar(50), c_lote varchar(50), c_fechaprod varchar(10), c_calibre varchar(50),
c_corte varchar(50), c_terminacion varchar(50), c_calidad varchar(50), c_convervacion varchar(50),
c_unidad varchar(50), c_piezas varchar(3), c_kilos varchar(5), c_usuario varchar(50))



INSERT INTO @TablaEtiqueta(IdCaja, c_especie, c_empresa,c_turno,c_lote,c_fechaprod,c_calibre,
c_corte, c_terminacion, c_calidad, c_convervacion, c_unidad, c_piezas, c_kilos, c_usuario)

EXEC SP_CamposEtiquetaTRN 0


SET @especie = (Select c_especie from @TablaEtiqueta where IdCaja = @IdCaja)
SET @especie = ISNULL(@especie,'')

SET @empresa = (Select c_empresa from @TablaEtiqueta where IdCaja = @IdCaja)
SET @empresa = ISNULL(@empresa,'')

SET @turno = (Select c_turno from @TablaEtiqueta where IdCaja = @IdCaja)
SET @turno = ISNULL(@turno,'')

SET @lote = (Select c_lote from @TablaEtiqueta where IdCaja = @IdCaja)
SET @lote = ISNULL(@lote,'')

SET @fechaprod = (Select c_fechaprod from @TablaEtiqueta where IdCaja = @IdCaja)
SET @fechaprod = ISNULL(@fechaprod,'')

SET @calibre = (Select c_calibre from @TablaEtiqueta where IdCaja = @IdCaja)
SET @calibre = ISNULL(@calibre,'')

SET @corte = (Select c_corte from @TablaEtiqueta where IdCaja = @IdCaja)
SET @corte = ISNULL(@corte,'')

SET @terminacion = (Select c_terminacion from @TablaEtiqueta where IdCaja = @IdCaja)
SET @terminacion = ISNULL(@terminacion,'')

SET @calidad = (Select c_calidad from @TablaEtiqueta where IdCaja = @IdCaja)
SET @calidad = ISNULL(@calidad,'')

SET @conservacion = (Select c_convervacion from @TablaEtiqueta where IdCaja = @IdCaja)
SET @conservacion = ISNULL(@conservacion,'')

SET @Unidad = (Select c_unidad from @TablaEtiqueta where IdCaja = @IdCaja)
SET @Unidad = ISNULL(@Unidad,'')

SET @piezas = (Select c_piezas from @TablaEtiqueta where IdCaja = @IdCaja)
SET @piezas = ISNULL(@piezas,'')

SET @kilos = (Select c_kilos from @TablaEtiqueta where IdCaja = @IdCaja)
SET @kilos = ISNULL(@kilos,'')

SET @usuario = (Select c_usuario from @TablaEtiqueta where IdCaja = @IdCaja)
SET @usuario = ISNULL(@usuario,'')

SET @fechaimpresion = (select Format(CURRENT_TIMESTAMP,'dd-MM-yyyy HH:mm:ss'))

declare Etiqueta cursor for
	
	
Select str_text From det_etiqueta where (cod_meti IN(@IdEtiqueta)) 
ORDER BY cod_meti,n_linea
	

open Etiqueta


Fetch next from Etiqueta
into @str_text 
 
while @@fetch_status = 0
begin

	


	select @str_linea = replace(@str_text,'#CBARRA', @IdCaja)
	select @str_linea = replace(@str_linea,'#ESPECIE', @especie)
	select @str_linea = replace(@str_linea,'#TURNO', @turno)
	select @str_linea = replace(@str_linea,'#LOTE', @lote)
	select @str_linea = replace(@str_linea,'#FCH_PROD', @fechaprod)
	select @str_linea = replace(@str_linea,'#CALIBRE', @calibre)
	select @str_linea = replace(@str_linea,'#CORTE', @corte)
	select @str_linea = replace(@str_linea,'#TERMINACION', @terminacion)
	select @str_linea = replace(@str_linea,'#CALIDAD', @calidad)
	select @str_linea = replace(@str_linea,'#CONSERVACION', @conservacion)
	select @str_linea = replace(@str_linea,'#UNIDAD', @unidad)
	select @str_linea = replace(@str_linea,'#PIEZAS', @piezas)
	select @str_linea = replace(@str_linea,'#KILOS', @kilos)
	select @str_linea = replace(@str_linea,'#USUARIO', @usuario)
	select @str_linea = replace(@str_linea,'#FCH_IMPRESION', @fechaimpresion)

	

	Insert into @TablaTemporal(idlinea,texto)
	Values (0, @str_linea)



	fetch next from Etiqueta into @str_text

		
end
	
close Etiqueta
	
deallocate Etiqueta

Select texto From @TablaTemporal



END
GO